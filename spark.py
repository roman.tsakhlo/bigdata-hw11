from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.window import Window
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, DoubleType, LongType, ArrayType
import json

# !!! 17.14.11 - yy.dd.mm 
# !!! category_name: String, - alias 
# video_id,trending_date,title,channel_title,category_id,publish_time,tags,views, likes,dislikes,
# comment_count,thumbnail_link,comments_disabled,ratings_disabled,video_error_or_removed

# ----------------- 

import pandas as pd

df = pd.read_csv("FRvideos.csv")

filtered_df = df[df['video_id'].str.len() == 11]
df['views'] = pd.to_numeric(df['views'])
filtered_df = df.drop(columns=['description'])

# Записуємо новий CSV-файл
filtered_df.to_csv("FRvideosNew.csv", index=False)

# ----------------- General 

csv_path = "FRvideosNew.csv"
spark = SparkSession.builder.appName("YouTubeFR").getOrCreate()
df = spark.read.csv(csv_path, header=True, inferSchema=True)

# ----------------- 1 

trending_days = df.groupBy("video_id").agg(F.count("trending_date").alias("trending_days"))
df_join = df.join(trending_days, "video_id")

top_trending_videos = df_join.groupBy("video_id", "title").agg(
    F.max("views").alias("latest_views"),
    F.max("likes").alias("latest_likes"),
    F.max("dislikes").alias("latest_dislikes"),
    F.collect_list(F.struct("trending_date", "views", "likes", "dislikes")).alias("trending_days")
).orderBy(F.desc("trending_days")).limit(10)

result_json = top_trending_videos.toJSON().collect()
result_list = json.loads('[' + ','.join(result_json) + ']')

with open("./jsons/1.json", 'w') as json_file:
    json.dump({"videos": result_list}, json_file)


# ----------------- 2 

selected_columns = ["video_id", "trending_date", "category_id", "channel_title", "views"]

df = df.withColumn("trending_date", F.to_date(F.col("trending_date"), "yy.dd.MM"))

df = df.withColumn("week_start_date", F.expr("date_sub(trending_date, dayofweek(trending_date) - 1)"))
df = df.withColumn("week_end_date", F.expr("date_add(week_start_date, 6)"))

window_spec = Window.partitionBy("video_id").orderBy("trending_date")

df = df.withColumn("prev_views", F.lag("views").over(window_spec))
df = df.withColumn("new_views", F.when(F.isnull(df["prev_views"]), df["views"]).otherwise(df["views"] - df["prev_views"]))

result_df = df.groupBy("week_start_date", "week_end_date", "category_id").agg(
    F.first("channel_title").alias("channel_name"),
    F.first("week_start_date").alias("start_date"),
    F.first("week_end_date").alias("end_date"),
    F.count("video_id").alias("number_of_videos"),
    F.sum("new_views").alias("total_views"),
    F.collect_list("video_id").alias("video_ids")
)

result_df = result_df.select("channel_name", "start_date", "end_date", "number_of_videos", "total_views", "video_ids")

result_json = result_df.toJSON().collect()
result_list = json.loads('[' + ','.join(result_json) + ']')

with open("./jsons/2.json", 'w') as json_file:
    json.dump({"weeks": result_list}, json_file)

# ----------------- 3  

selected_columns = ["video_id", "trending_date", "tags"]

df = df.withColumn("trending_date", F.to_date(F.col("trending_date"), "yy.dd.MM"))

months_df = df.select("video_id", "trending_date", F.explode(F.split("tags", "\\|")).alias("tag"))

window_spec = Window.partitionBy("trending_date").orderBy(F.desc("tag"))
ranked_tags_df = months_df.withColumn("rank", F.rank().over(window_spec)).filter(F.col("rank") <= 10)

result_df = ranked_tags_df.groupBy("trending_date").agg(
    F.first("trending_date").alias("start_date"),
    F.last("trending_date").alias("end_date"),
    F.collect_list(
        F.struct("tag", "rank", "video_id")
    ).alias("tags_info")
)

result_df = result_df.select("start_date", "end_date", "tags_info")

result_json = result_df.toJSON().collect()
result_list = json.loads('[' + ','.join(result_json) + ']')

with open("./jsons/3.json", 'w') as json_file:
    json.dump({"months": result_list}, json_file)

# ----------------- 4  

selected_columns = ["video_id", "title", "channel_title", "views", "publish_time"]

df = df.withColumn("publish_time", F.to_timestamp(F.col("publish_time"), "yyyy-MM-dd'T'HH:mm:ss.SSSX"))

window_spec = Window.partitionBy("channel_title", "video_id").orderBy(F.desc("publish_time"))
ranked_df = df.withColumn("rank", F.row_number().over(window_spec)).filter(F.col("rank") == 1)

window_spec_channel = Window.partitionBy("channel_title").orderBy(F.desc("views"))
ranked_channel_df = ranked_df.withColumn("rank_channel", F.rank().over(window_spec_channel)).filter(F.col("rank_channel") <= 20)

result_df = ranked_channel_df.groupBy("channel_title").agg(
    F.first("channel_title").alias("channel_name"),
    F.first("publish_time").alias("start_date"),
    F.last("publish_time").alias("end_date"),
    F.sum("views").alias("total_views"),
    F.collect_list(
        F.struct("video_id", "views")
    ).alias("videos_views")
)

result_df = result_df.select("channel_name", "start_date", "end_date", "total_views", "videos_views")

result_json = result_df.toJSON().collect()
result_list = json.loads('[' + ','.join(result_json) + ']')

with open("./jsons/4.json", 'w') as json_file:
    json.dump({"channels": result_list}, json_file)

# ----------------- 5  

hardcoded_trending_days = ["18.01.05", "18.02.05", "18.03.05", "18.04.05", "18.25.05", "18.26.05", "18.30.05", "18.31.05"]
selected_columns = ["video_id", "trending_date", "title", "views", "likes", "dislikes"]

trending_days_df = spark.createDataFrame([(t, ) for t in hardcoded_trending_days], ["trending_date"]) 

df = df.join(trending_days_df, on="trending_date", how="inner")

trending_days_df = df.groupBy("video_id", "title", "channel_title").agg(
    F.countDistinct("trending_date").alias("trending_days")
)

window_spec = Window.partitionBy("channel_title").orderBy(F.desc("trending_days"))
ranked_df = trending_days_df.withColumn("rank", F.rank().over(window_spec)).filter(F.col("rank") <= 10)

result_df = ranked_df.groupBy("channel_title").agg(
    F.first("channel_title").alias("channel_name"),
    F.sum("trending_days").alias("total_trending_days"),
    F.collect_list(
        F.struct("video_id", "title", "trending_days")
    ).alias("videos_days")
)

result_df = result_df.select("channel_name", "total_trending_days", "videos_days")

result_json = result_df.toJSON().collect()
result_list = json.loads('[' + ','.join(result_json) + ']')

with open("./jsons/5.json", 'w') as json_file:
    json.dump({"channels": result_list}, json_file)

# ----------------- 6  

df = spark.read.csv(csv_path, header=True, inferSchema=True)

selected_columns = ["category_id", "video_id", "title", "likes", "dislikes", "views"]

df = df.withColumn("ratio_likes_dislikes", F.col("likes") / F.greatest(F.col("dislikes"), F.lit(1)))
df_filtered = df.filter(F.col("views") > 100000)
window_spec = Window.partitionBy("category_id").orderBy(F.desc("ratio_likes_dislikes"))
ranked_df = df_filtered.withColumn("rank", F.rank().over(window_spec)).filter(F.col("rank") <= 10)

category_schema = StructType([
    StructField("category_id", IntegerType(), True),
    StructField("videos", ArrayType(StructType([
        StructField("video_id", StringType(), True),
        StructField("video_title", StringType(), True),
        StructField("ratio_likes_dislikes", DoubleType(), True),
        StructField("views", LongType(), True)
    ]), True), True)
])

result_df = ranked_df.groupBy("category_id").agg(
    F.collect_list(
        F.struct("video_id", "title", "ratio_likes_dislikes", "views")
    ).alias("videos")
)

result_df = result_df.select("category_id", "videos")

result_json = result_df.toJSON().collect()
result_list = json.loads('[' + ','.join(result_json) + ']')

with open("./jsons/6.json", 'w') as json_file:
    json.dump({"categories": result_list}, json_file)

# ----------------- 
    
spark.stop()

